# tbc-docker-kaniko-cache-bug

```
#KANIKO_SNAPSHOT_IMAGE_CACHE=will_win

function test_function() {
    DOCKER_SNAPSHOT_IMAGE=$1
    kaniko_snapshot_image_cache="${KANIKO_SNAPSHOT_IMAGE_CACHE:-${DOCKER_SNAPSHOT_IMAGE%:*}/cache}"
    echo $DOCKER_SNAPSHOT_IMAGE
    echo $kaniko_snapshot_image_cache
    echo 
}

test_function host.name/group/project:tag
test_function host.name/group/project
test_function host.name:port/group/project:tag
test_function host.name:port/group/project

test_function registry.gitlab.com/michaelkebe/tbc-docker-kaniko-cache-bug/snapshot:main
test_function registry.gitlab.com/michaelkebe/tbc-docker-kaniko-cache-bug/snapshot

test_function registry.gitlab.com:443/michaelkebe/tbc-docker-kaniko-cache-bug/snapshot:main
test_function registry.gitlab.com:443/michaelkebe/tbc-docker-kaniko-cache-bug/snapshot
```
